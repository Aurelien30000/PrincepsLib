package biz.princeps.lib.manager;

import biz.princeps.lib.PrincepsLib;
import biz.princeps.lib.storage_old.DatabaseAPI;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by spatium on 17.07.17.
 */
public abstract class Manager {

    protected final JavaPlugin plugin;
    protected final DatabaseAPI api;

    public Manager(DatabaseAPI api) {
        this.plugin = PrincepsLib.getPluginInstance();
        this.api = api;
    }

}
