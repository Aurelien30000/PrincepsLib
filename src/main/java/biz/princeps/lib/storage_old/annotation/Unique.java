package biz.princeps.lib.storage_old.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by spatium on 17.07.17.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Unique {
}
