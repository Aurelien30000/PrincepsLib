package biz.princeps.lib.storage_old;

/**
 * Created by spatium on 17.07.17.
 */
public enum DatabaseType {

    SQLite,
    H2, MySQL
}
