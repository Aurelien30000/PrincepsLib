package biz.princeps.lib.broadcasting;

import biz.princeps.lib.PrincepsLib;
import com.google.common.util.concurrent.FutureCallback;
import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Project: PrincepsLib
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 06/18/17
 * <p>
 * This is the actual discord class.
 * It basically said just wraps around btobastians javacordAPI
 */
public class Discord {

    private final String token;
    private final String channelid;
    private final String gamename;
    private DiscordAPI discord;

    public Discord(String token, String channelid, String gamename) {
        this.token = token;
        this.channelid = channelid;
        this.gamename = gamename;
    }

    /**
     * Call this method to connect to discord.
     * This method MUST be called in order to use the bot.
     */
    public void connect() {
        discord = Javacord.getApi(token, true);
        discord.connect(new FutureCallback<DiscordAPI>() {

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }

            @Override
            public void onSuccess(DiscordAPI api) {
                discord = api;
                discord.setGame(gamename);
            }
        });
    }

    /**
     * Sends a message asynchronously to the given channel
     *
     * @param message the message, which should be sent
     */
    public void sendMessage(String message) {
        new BukkitRunnable() {

            @Override
            public void run() {
                discord.getChannelById(channelid).sendMessage(message);
                cancel();
            }
        }.runTaskAsynchronously(PrincepsLib.getPluginInstance());
    }

    /**
     * Disconnects the bot from discord
     */
    public void disconnect() {
        discord.disconnect();
    }


}
