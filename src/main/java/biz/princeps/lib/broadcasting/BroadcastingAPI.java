package biz.princeps.lib.broadcasting;

/**
 * Project: PrincepsLib
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 07/20/17
 * <p>
 * This class can be used to instantiate a new broadcasting service
 * Available are: discord, skype, telegram
 */
public class BroadcastingAPI {

    private static Discord discord;
    private static PSkype skype;
    private static Telegram telegram;

    /**
     * Returns the instance of the discord bot.
     * Need to call BroadcastingAPI#createDiscord first
     *
     * @return the instance
     */
    public static Discord getDiscord() {
        return discord;
    }

    /**
     * Returns the instance of the skype bot.
     * Need to call BroadcastingAPI#createSkype first
     *
     * @return the instance
     */
    public static PSkype getSkype() {
        return skype;
    }

    /**
     * Returns the instance of the telegram bot.
     * Need to call BroadcastingAPI#createTelegram first
     *
     * @return the instance
     */
    public static Telegram getTelegram() {
        return telegram;
    }


    /**
     * Creates a new instance of a discord bot.
     * This bot is able to send messages.
     *
     * @param token     the discord token, which is required to connect
     * @param channelid the channelid (leftclick on a channel to display it)
     * @param gamename  the name of the game the bot is supposed to play
     * @return a fresh instance
     */
    public static Discord createDiscord(String token, String channelid, String gamename) {
        discord = new Discord(token, channelid, gamename);
        return discord;
    }

    /**
     * Creates a new instance of a skype bot.
     * This bot is able to send messages.
     *
     * @param username  the username of the bot account
     * @param password  the password of the bot account
     * @param groupname the groupname messages should be sent to
     * @return a fresh instance
     */
    public static PSkype createSkype(String username, String password, String groupname) {
        skype = new PSkype(username, password, groupname);
        return skype;
    }

    /**
     * Creates a new instance of a telegram bot.
     * This bot is able to send messages.
     *
     * @param auth   the auth code of the bot
     * @param chatid the chatid messages should be sent to
     * @return a fresh instance
     */
    public static Telegram createTelegram(String auth, String chatid) {
        telegram = new Telegram(auth, chatid);
        return telegram;
    }

}
