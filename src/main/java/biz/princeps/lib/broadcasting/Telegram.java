package biz.princeps.lib.broadcasting;

import biz.princeps.lib.PrincepsLib;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.net.URL;

/**
 * Project: PrincepsLib
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 07/20/17
 * <p>
 * This is the actual telegram class.
 * It uses telegram web API, which allows to send messages in a very easy way.
 * Simply open a stream to api.telegram.org and call a specific method there
 */
public class Telegram {

    private final String auth;
    private final String chatid;

    public Telegram(String auth, String chatid) {
        this.auth = auth;
        this.chatid = chatid;
    }

    /**
     * Send a message via telegram to a defined chat
     * Define your chat in the constructor beforehand.
     * You don't have to connect to telegram or anything, because telegram provides a nice auth based web api
     *
     * @param msg this is your message you want to send.
     */
    public void sendMessage(String msg) {
        StringBuilder url = new StringBuilder();
        url.append("https://api.telegram.org/bot")
                .append(auth)
                .append("/sendMessage?chat_id=")
                .append(chatid)
                .append("&text=")
                .append(msg);

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    new URL(url.toString()).openStream().close();
                } catch (IOException e) {
                    PrincepsLib.getPluginInstance().getLogger().warning("Could not send message via telegram: " + e);
                }
                cancel();
            }

        }.runTaskAsynchronously(PrincepsLib.getPluginInstance());
    }


}
