package biz.princeps.lib.broadcasting;

import biz.princeps.lib.PrincepsLib;
import fr.delthas.skype.Group;
import fr.delthas.skype.Presence;
import fr.delthas.skype.Skype;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;

/**
 * Project: PrincepsLib
 * Created by Alex D. (SpatiumPrinceps)
 * Date: 06/18/17
 * <p>
 * This is the actual skype class.
 * It basically said just wraps around delthas skype-java api
 */
public class PSkype {

    private Skype skype;
    private final String username;
    private final String password;
    private final String groupname;

    public PSkype(String username, String password, String groupname) {
        this.username = username;
        this.password = password;
        this.groupname = groupname;
    }

    /**
     * Call this method to connect to skype.
     * This method MUST be called in order to use the bot.
     */
    public void connect() {
        new BukkitRunnable() {

            @Override
            public void run() {
                skype = new Skype(username, password);
                try {
                    skype.connect();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                skype.changePresence(Presence.ONLINE);
                if (skype.isConnected())
                    PrincepsLib.getPluginInstance().getLogger().info("Skype-Connection established!");
            }
        }.runTaskAsynchronously(PrincepsLib.getPluginInstance());
    }

    /**
     * Sends a message asynchronously to the given chat
     *
     * @param message the message, which should be sent
     */
    public void sendMessage(String message) {
        new BukkitRunnable() {

            @Override
            public void run() {
                if (skype != null)
                    if (skype.isConnected()) {
                        for (Group chat : skype.getGroups()) {
                            if (chat.getTopic().equals(groupname))
                                chat.sendMessage(message);
                        }
                    }
                cancel();
            }
        }.runTaskAsynchronously(PrincepsLib.getPluginInstance());
    }
}
